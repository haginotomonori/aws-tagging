```bash
docker build ./docker -t tag-check
docker run -it -v $(pwd):/app tag-check

# aws config
root@3aee20fc87cf:/app# aws configure
AWS Access Key ID [None]: xxxxxxxx
AWS Secret Access Key [None]: xxxxxxxxxx
Default region name [None]:
Default output format [None]:

cp config/aws_config ~/.aws/config

# 実行
sh bin/tag_check.sh > storage/tag_check.txt
```
IAM hagino-tag-check-user を使います。権限が足らなければ適宜追加してください

# memo
## 対象サービス
* ec2(instance)
* rds
* cf
* s3

必要あれば追加してください

## 対象リージョン
* ap-northeast-1
* us-east-1
* us-east-2
* us-west-1
* us-west-2

必要あれば追加してください

## links
[AWS CLI Command Reference](https://docs.aws.amazon.com/cli/latest/reference/)

# tagづけ

addTagとかputTagとかいうAPIをドキュメントから探して叩けばいける  
試しにs3で試してみたら、既存のTagが消えてしまったので実験してから一括実行するのがいいかも

## s3
```bash
aws s3api put-bucket-tagging --bucket tsubasa-storage --tagging 'TagSet=[{Key=ChorusCost_Tag1,Value=tsubasa}, {Key=ChorusCost_Tag2,Value=development}]'

# before
tsubasa-storage | {"Project":"tsubasa"}

# after
tsubasa-storage | {"ChorusCost_Tag1":"tsubasa"} {"ChorusCost_Tag2":"development"}
```
