#!/bin/bash

get_ec2_info () {
    region=$1
    responses=$(aws --region ${region} ec2 describe-instances)
    echo "--- "$region" ---"
    echo $responses | jq -r '.Reservations[].Instances[] | [.InstanceId, (.Tags[] | .Key + "=" + .Value)] | @csv'
}

get_rds_tags () {
    region=$1
    echo "---rds $region ---"
    for arn in `aws --region $region rds describe-db-instances | jq -r .DBInstances[].DBInstanceArn`; do
        tags=$(aws --region $region rds list-tags-for-resource --resource-name $arn | jq -c '.[][] | {(.Key): .Value}' | tr '\n' '\t')
        echo $arn ',' $tags
    done

    # aurora
    for arn in `aws --region $region rds describe-db-clusters | jq -r .DBClusters[].DBClusterArn`; do
        tags=$(aws --region $region rds list-tags-for-resource --resource-name $arn | jq -c '.[][] | {(.Key): .Value}' | tr '\n' '\t')
        echo $arn ',' $tags
    done
}

get_cf_tags() {
    region=$1
    echo "---cf $region ---"
    for cloudfront in `aws --region ${region} cloudfront list-distributions --query "DistributionList.Items[].ARN" | jq .[] | tr -d \"`; do
        tags=$(aws cloudfront list-tags-for-resource --resource $cloudfront | jq -c '.[][][] | {(.Key): .Value}' | tr '\n' '\t')
        echo $cloudfront '|' $tags
    done
}

get_s3_tags() {
    echo "---s3 ---"
    for bucket in `aws s3api list-buckets | jq .Buckets[].Name | tr -d \"`; do
        tags=$(aws s3api get-bucket-tagging --bucket $bucket | jq -c '.[][] | {(.Key): .Value}' | tr '\n' '\t')
        echo $bucket '|' $tags
    done
}

## main
# ec2 rds cf
for region in "ap-northeast-1" "us-east-1" "us-east-2" "us-west-1" "us-west-2"; do
    get_ec2_info $region
    get_rds_tags $region
    get_cf_tags $region
done

# s3
get_s3_tags

echo "done"